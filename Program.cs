﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge
{
    class Program
    {

        private static Pyramid[] ReadInputs(TextReader input)
        {
            //Add try catch
            string line = input.ReadLine();
            int noOfPyramids = 1;
            var result = int.TryParse(line, out noOfPyramids);
            if (!result || noOfPyramids < 1 || noOfPyramids > 256)
            {
                PrintInvalidInput();
                return null;
            }
            line = input.ReadLine();
            Pyramid[] pyramids = new Pyramid[noOfPyramids];
            int i = 0;

            while (!string.IsNullOrEmpty(line))
            {
                var pyramid = new Pyramid();

                var inputs = line.Split(' ');
                if (inputs.Length != 3)
                {
                    PrintInvalidInput();
                    return null;
                }
                //String               
                if (inputs[0].Length < 1 || inputs[0].Length > 16)
                {
                    PrintInvalidInput();
                    return null;
                }
                pyramid.pStr = inputs[0];

                //Height                
                result = int.TryParse(inputs[1], out pyramid.h);
                if (!result || pyramid.h < 1 || pyramid.h > 128)
                {
                    PrintInvalidInput();
                    return null;
                }

                //Direction       

                result = int.TryParse(inputs[2], out pyramid.direction);
                if (!result || (pyramid.direction != -1 && pyramid.direction != 1))
                {
                    PrintInvalidInput();
                    return null;
                }
                // Add to collection
                if (i < pyramids.Length)
                {
                    pyramids[i] = pyramid;
                    i++;
                }

                line = input.ReadLine();
            }

            return pyramids;
        }
        static void Main(string[] args)
        {

            var pyramids = ReadInputs(Console.In);
            if (pyramids == null || pyramids.Length == 0)
            {
                Console.WriteLine("No Pyramids Input Found");                 
            }
            else
            {
                PrintPyramids(pyramids);
            }

            Console.ReadLine();
        }

        private static void PrintPyramids(Pyramid[] pyramids)
        {
           

            int directionIndicator = 1, noOfSpaces = 3;         
           
            int i = -1;            
            int topCursorPosition = Console.CursorTop +1;            

            for (int p = 0; p < pyramids.Length; p++)
            {
                var maxpos = pyramids[p].h * 2 + 1;
                int leftCursorPosition = (maxpos - 1) / 2;

                //Set START and END of Pyramid as per Direction
                int h = pyramids[p].direction == 1 ? 0 : pyramids[p].h - 1;
                int hEnd = pyramids[p].direction == 1 ? pyramids[p].h - 1 : 0;
            
                    int hLoop = h;
                    while (true)
                    {
                        //Exit if Pyramid is formed as per Direction
                        if(pyramids[p].direction == 1 && hLoop > hEnd)
                        {
                            break;
                        }
                        else if (pyramids[p].direction == -1 && hLoop < hEnd)
                        {
                            break;
                        }
                        //Print line of Pyramid in current direction.
                        string currentString = string.Empty;
                        int numberOfChars = hLoop * 2 + 1; //Determine chars for current line
                       // currentString = currentString + pyramids[p].pStr[i];
                        for (int j = 0; j < numberOfChars; j++)
                        {
                            SetNextIndex(ref i, directionIndicator, pyramids[p].pStr);
                            currentString = currentString + pyramids[p].pStr[i];
                        }
                        Console.SetCursorPosition(leftCursorPosition - hLoop, topCursorPosition);
                    string blanks = string.Empty;
                        for(int s=0; s< leftCursorPosition - hLoop-1; s++) { blanks = blanks + " "; }
                        Console.WriteLine(blanks + currentString);
                        directionIndicator *= -1;
                        //for (int k = 0; k < noOfSpaces; k++)
                        //{
                        //    SetNextIndex(ref i, directionIndicator, pyramids[p].pStr);
                        //}
                        topCursorPosition++;
                        hLoop = hLoop + pyramids[p].direction;
                    }               
                
            }
        }

        private static void PrintInvalidInput()
        {
            Console.WriteLine("Invalid Input");
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static void SetNextIndex(ref int i, int directionIndicator, string s)
        {
            if(i==-1) { i = 0; return; }
            if (i + directionIndicator >= s.Length) { i = 0; return; }
            if (i + directionIndicator == -1) { i = s.Length - 1; return; }           
            i += directionIndicator;           
        }

        public struct Pyramid
        {
            public string pStr;
            public int h;
            public int direction;
        }
    }
}
